<?xml version="1.0"?>

<!-- ======================================================================= -->
<!-- Alfresco build environment -->
<!-- ======================================================================= -->

<!-- PLEASE CHANGE THIS PROJECT NAME! -->
<project name="Alfresco OpenId Authenticator" default="build" basedir=".">

    <import file="minify.xml" />
    
	<!-- include ant-contrib.jar -->
	<taskdef resource="net/sf/antcontrib/antlib.xml">
		<classpath>
			<pathelement location="./../lib/ant-contrib-1.0b3.jar" />
		</classpath>
	</taskdef>

	<!-- bring through environment variables -->
	<property environment="env" />

	<!-- read the properties file to override any of these defaults -->
	<property file="build.properties" />

	<!-- detect what type of formatter to use -->
	<condition property="junit.formatter" value="xml" else="plain">
		<equals arg1="${env.BUILD_ENVIRONMENT}" arg2="ci" />
	</condition>

	<!-- see if the project has the alfresco libraries locally, otherwise set 
		a value and indicate to copy them across -->
	<available
		file="${runtime.dir}/WEB-INF/classes/alfresco/application-context.xml"
		type="file" property="alfresco.build.exists" />

	<available file="${basedir}/custom.properties" type="file"
		property="custom.properties.exists" />

	<fail unless="custom.properties.exists"
		message="Please create a custom.properties file in ${basedir} (a sample file is available in that directory)." />

	<!-- the custom configuration properties if set. These are read below and 
		overridden if the values are supplied by the command line instead -->
	<property file="custom.properties" />

	<!-- see if we need to copy the .sample configs to real ones -->
	<available file="${alfresco.config.dir}/alfresco-global.properties"
		type="file" property="alfresco-global-properties" />

	<path id="all-classpath">
		<fileset dir="${lib.dir}">
			<include name="**/*.jar" />
		</fileset>
		<fileset dir="${run.tomcat.lib}">
			<include name="**/*.jar" />
		</fileset>
		<fileset dir="${runtime.lib}">
			<include name="**/*.jar" />
		</fileset>
	</path>

	<target name="init" depends="copy-alfresco">
		<tstamp>
			<format property="TODAY" pattern="d-MM-yy" />
		</tstamp>
	</target>

	<target name="copy-alfresco" unless="alfresco.build.exists">
		<echo>Alfresco libraries and configuration not found at
			${runtime.dir}, copying from ${alfresco.location}</echo>
		<mkdir dir="${runtime.dir}" />
		<copy todir="${runtime.dir}">
			<fileset dir="${alfresco.location}">
				<include name="**/*" />
			</fileset>
		</copy>
	</target>

	<!-- =================================================================== -->
	<!-- Compiles all classes and creates a jar file -->
	<!-- =================================================================== -->
	<target name="compile-classes" depends="init">
		<mkdir dir="${build.webinf.dir}" />
		<mkdir dir="${build.classes.dir}" />

		<javac destdir="${build.classes.dir}" debug="${build.debug}"
			deprecation="off" optimize="${build.optimize}" source="${build.java_version}"
			target="${build.java_version}" includes="**/*.java">
			<src path="${src.java}" />
			<classpath refid="all-classpath" />
		</javac>
	</target>


	<target name="deploy-local-webscripts">
		<copy todir="${runtime.classes}/alfresco/extension/templates/webscripts">
			<fileset dir="${config.webapp}/alfresco/extension/templates/webscripts">
				<include name="**/*" />
				<exclude name="**/*.svn" />
			</fileset>
		</copy>
	</target>

	<target name="deploy-web-tests">
		<copy todir="${runtime.dir}">
			<fileset dir="${src.test-resources.webroot.dir}">
				<include name="**/*" />
				<exclude name="**/*.svn" />
			</fileset>
		</copy>
	</target>

	<target name="create-jar">
		<echo>Creating jar ${jar.file}</echo>
		<mkdir dir="${build.lib.dir}" />
		<mkdir dir="${build.dir}/META-INF" />
		<jar destfile="${jar.file}">
			<metainf dir="${build.dir}/META-INF" />
			<fileset dir="${build.classes.dir}" excludes="**/*Test*"
				includes="**/*.class" />
			<fileset dir="${src.java}" excludes="**/*.java" includes="**/*" />
		</jar>
		<delete dir="${build.dir}/META-INF" />
	</target>

	<target name="copy-web-resources">
		<copy todir="${build.dir}">
			<fileset dir="${src.web}">
				<include name="**/*" />
				<exclude name="**/*.svn" />
			</fileset>
		</copy>
	</target>

	<target name="copy-lib">
		<copy todir="${build.lib.dir}">
			<fileset dir="${lib.dir}">
				<include name="**/*" />
				<exclude name="servlet-api.jar" />
				<exclude name="**/*.svn" />
			</fileset>
			<!-- messy hack to make sure that ant doesn't magically mysteriously delete 
				certain alfresco .jar files when it runs!! Seriously, wth? <fileset dir="${runtime.lib}"> 
				<include name="**/*" /> </fileset> -->
		</copy>
	</target>

	<target name="copy-config">
		<echo>Copying custom configuration from ${alfresco.config.dir} </echo>
		<available file="${alfresco.config.dir}/alfresco-global.properties"
			property="isFileAvail" />
		<fail unless="isFileAvail"
			message="Configuration file ${alfresco.config.dir}/alfresco-global.properties does not exist." />

		<copy todir="${build.classes.dir}">
			<fileset dir="${config.webapp}">
				<include name="**/*" />
				<exclude name="**/*.svn" />
				<exclude name="**/log4j.properties" />
			</fileset>
			<fileset dir="${alfresco.config.dir}">
				<include name="**/*" />
				<exclude name="**/*.svn" />
			</fileset>
		</copy>
	</target>

	<target name="copy-extension" unless="alfresco.version.3.2">
		<!-- copy custom configuration files if needed -->
		<echo>Copying extension configuration</echo>
		<copy todir="${alfresco.extension}">
			<fileset dir="${alfresco.config.dir}">
				<include name="**/*" />
				<exclude name="**/*.svn" />
				<exclude name="**/*.sample" />
				<exclude name="**/alfresco-global.properties" />
				<exclude name="**/log4j.properties" />
			</fileset>
		</copy>
	</target>

	<!-- deploys to the local runtime 'deployed' directory which is used to 
		actually run the application -->
	<target name="deploy-local">
		<copy todir="${runtime.dir}" includeEmptyDirs="false">
			<fileset dir="${build.dir}">
				<exclude name="**/*.class" />
				<include name="**/*" />
			</fileset>
		</copy>
	</target>

	<target name="build-base"
		depends="compile-classes,create-jar,copy-config,copy-extension,copy-lib,copy-web-resources,deploy-local">
	</target>

	<target name="build"
		depends="build-base,dupe-min-files,copy-files-to-build-deployed">
	</target>

	<!-- Builds the module -->
	<target name="module"
		depends="build-base,minify-web-resources,copy-files-to-build-deployed">
		<delete file="${amp.file}" />

		<property name="module.lib.dir" value="${build.module}/lib" />
		<property name="module.web.dir" value="${build.module}/web" />
		<property name="module.config.dir" value="${build.module}/config" />

		<!-- copy the relevant build files to the module dir in the correct location -->
		<copy file="${jar.file}" todir="${module.lib.dir}" />

		<!-- copy any lib dependencies -->
		<copy todir="${module.lib.dir}">
			<fileset dir="${lib.dir}">
				<include name="**/*" />
				<exclude name="**/*.svn" />
			</fileset>
		</copy>

		<!-- copy stuff for the module's configuration dirs -->
		<copy todir="${module.config.dir}">
			<fileset dir="${config.webapp}">
				<include name="**/*" />
				<exclude name="**/*.svn" />
				<exclude name="**/module.properties" />
				<exclude name="**/file-mapping.properties" />
				<exclude name="**/log4j.properties" />
			</fileset>
		</copy>

		<!-- copy the module.properties because it needs to be defined at the top 
			level instead for the AMP tool to work -->
		<copy todir="${build.module}"
			file="${build.classes.dir}/alfresco/module/${module.name}/module.properties" />
		<copy todir="${build.module}"
			file="${build.classes.dir}/alfresco/module/${module.name}/file-mapping.properties" />

		<!-- copy any web folders too -->
		<copy todir="${module.web.dir}">
			<fileset dir="${src.web}">
				<include name="**/*" />
				<exclude name="**/*.svn" />
				<exclude name="**/WEB-INF*" />
			</fileset>
		</copy>

		<zip destfile="${amp.file}">
			<fileset dir="${build.module}">
				<include name="**/*" />

			</fileset>
		</zip>
	</target>

	<!-- =================================================================== -->
	<!-- Distribution target -->
	<!-- Create a distribution package that should be able to be extracted -->
	<!-- over a "TOMCAT" directory and place files accordingly -->
	<!-- =================================================================== -->
	<target name="dist">
		<echo>To be implemented!</echo>
	</target>

	<!-- A target that will create the appropriate tomcat configuration context 
		and stick it in the execution tomcat -->
	<target name="run">
		<echo>Copying configuration to ${basedir}</echo>
		<!-- copy the configuration to the relevant catalina directory -->

	</target>

	<!-- =================================================================== -->
	<!-- Testing related targets -->
	<!-- =================================================================== -->
	<target name="build-tests" depends="compile-classes">
		<mkdir dir="${src.test-resources.classes.dir}" />
		<copy todir="${src.test-resources.classes.dir}">
			<fileset dir="${src.test-resources.config.dir}">
				<include name="config/**" />
				<!-- <include name="**/*.properties"/> -->
			</fileset>
			<fileset dir="${config.webapp}">
				<include name="**/*" />
			</fileset>
		</copy>

		<javac destdir="${src.test-resources.classes.dir}" debug="on"
			deprecation="off" optimize="off" source="1.6" includes="**/*.java">
			<src path="${src.test-resources.java.dir}" />
			<classpath refid="test-classpath" />
		</javac>
	</target>

	<target name="run-tests" depends="build-tests" description="Runs the projects unit tests">
		<mkdir dir="${src.test-resources.dir}" />
		<mkdir dir="${src.test-resources.dir}/results" />
		<junit printsummary="yes" fork="yes" maxmemory="128M"
			haltonfailure="no" dir="${src.test-resources.dir}">
			<!--<jvmarg value="-server"/> -->
			<classpath refid="test-classpath" />
			<formatter type="${junit.formatter}" />
			<batchtest todir="${src.test-resources.dir}/results">
				<fileset dir="${src.test-resources.java.dir}">
					<include name="com/**/Test*.java" />
				</fileset>
			</batchtest>
		</junit>
	</target>

	<target name="run-test" depends="build-tests"
		description="Runs a single test defined in the testcase property i.e. call this target with -Dtestcase=[test]">
		<mkdir dir="${src.test-resources.dir}" />
		<mkdir dir="${src.test-resources.dir}/results" />
		<junit printsummary="yes" fork="yes" maxmemory="128M"
			haltonfailure="no" dir="${src.test-resources.dir}">
			<!--<jvmarg value="-server"/> -->
			<classpath refid="test-classpath" />
			<formatter type="${junit.formatter}" />
			<test name="${testcase}" todir="${src.test-resources.dir}/results" />
		</junit>
	</target>

	<path id="test-classpath">
		<fileset dir="${lib.dir}">
			<include name="**/*.jar" />
		</fileset>
		<fileset dir="${runtime.lib}">
			<include name="**/*.jar" />
		</fileset>
		<pathelement path="${runtime.classes}" />
		<pathelement path="${src.test-resources.classes.dir}" />
	</path>
	<!-- =================================================================== -->
	<!-- Explains how to use this build file -->
	<!-- =================================================================== -->
	<target name="help" depends="init">
		<echo>

			USEFUL TARGETS
			===============
			The following useful targets are available (the default is compile).

			clean - cleans intermediate build products (does not remove built
			items from the deployed directory)
			build - performs a complete compile of the environment and deploys the
			files into the /deployed dir to be run by the Eclipse-tomcat plugin
			module - Creates an Alfresco Module Package (AMP) file for
			installation into a WAR file using the alfresco amps tool
			war - Creates a WAR file containing Alfresco AND any modifications made
			by this project
			run-tests - Runs all tests

		</echo>
	</target>

	<!-- =================================================================== -->
	<!-- Cleans out the project -->
	<!-- =================================================================== -->
	<target name="clean">
		<echo>Cleaning the build folders</echo>
		<delete dir="${build.dir}" />
	</target>

	<target name="clean-all">
		<delete file="${war.file}" />
		<echo>Deleting ${build.dir}</echo>
		<delete dir="${build.dir}" />
		<echo>Deleting ${runtime.dir}</echo>
		<delete dir="${runtime.dir}" />
	</target>	
		
	<property file="${basedir}/configs/alfresco/alfresco-global.properties"/>
	<target name="resetDatabase">
		<echo>Drop database , create new database and clean up alf_data</echo>
		<sql classpath="${lib.dir}/${mysql.driver}" driver="${db.driver}" url="${db.url}" userid="${db.username}" password="${db.password}" failOnConnectionError="false">
			DROP DATABASE IF EXISTS ${db.name};
			CREATE DATABASE ${db.name};
			USE ${db.name};
			GRANT SELECT ON ${db.name}.* TO ${db.username}@'${db.host}';
		</sql>
		<echo>Database is successfully reset</echo>
		<echo>Privileges granted to ${db.username}</echo>
		<echo>Cleaning files in ${dir.root}</echo>
		<delete>
			<fileset dir="${dir.root}"/>
		</delete>
		<echo>Database and Alf Data are reset</echo>
	</target>
	
	<target name="reset" depends="clean-all,resetDatabase"/>
	
	<target name="rsync-docker" >
		<exec executable="rsync" dir="${basedir}/deployed">
			<arg line="-av --chmod=a+rwx --delete --exclude .git . ${docker.user.name}@${docker.host}:${docker.location.alfresco}" />
		</exec>
		<exec executable="rsync" dir="${basedir}/..">
			<arg line="-av --chmod=a+rwx --exclude .git Dockerfile ${docker.user.name}@${docker.host}:${docker.location}" />
		</exec>
		<exec executable="rsync" dir="${basedir}/configs/alfresco">
			<arg line="-av --chmod=a+rwx --exclude .git alfresco-global.properties ${docker.user.name}@${docker.host}:${docker.location}" />
		</exec><!--
		<exec executable="chmod" dir="${basedir}/..">
			<arg line="a+rwx docker-run.sh" />
		</exec>-->
		<exec executable="chmod" dir="${basedir}/..">
			<arg line="a+rwx startup.sh" />
		</exec><!--
		<exec executable="dos2unix" dir="${basedir}/..">
			<arg line="docker-run.sh" />
		</exec>-->
		<exec executable="dos2unix" dir="${basedir}/..">
			<arg line="startup.sh" />
		</exec>
		<exec executable="rsync" dir="${basedir}/..">
			<arg line="-av --chmod=a+rwx --exclude .git startup.sh ${docker.user.name}@${docker.host}:${docker.location}" />
		</exec><!--
		<exec executable="rsync" dir="${basedir}/..">
			<arg line="-av -/-chmod=a+rwx -/-exclude .git docker-run.sh ${docker.user.name}@${docker.host}:${docker.location}" />
		</exec>-->
		<exec executable="rsync" dir="${basedir}/..">
			<arg line="-av --chmod=a+rwx --exclude .git support-files ${docker.user.name}@${docker.host}:${docker.location}" />
		</exec>
	</target>
	
	<target name="rsync-docker-preview" >
		<exec executable="rsync" dir="${basedir}/deployed">
			<arg line="-av --chmod=a+rwx --delete -n --exclude .git . ${docker.user.name}@${docker.host}:${docker.location.alfresco}" />
		</exec>
		<exec executable="rsync" dir="${basedir}/..">
			<arg line="-av --chmod=a+rwx -n --exclude .git Dockerfile ${docker.user.name}@${docker.host}:${docker.location}" />
		</exec>
		<exec executable="rsync" dir="${basedir}/configs/alfresco">
			<arg line="-av --chmod=a+rwx -n --exclude .git alfresco-global.properties ${docker.user.name}@${docker.host}:${docker.location}" />
		</exec>
		<exec executable="rsync" dir="${basedir}/..">
			<arg line="-av --chmod=a+rwx -n --exclude .git startup.sh ${docker.user.name}@${docker.host}:${docker.location}" />
		</exec><!--
		<exec executable="rsync" dir="${basedir}/..">
			<arg line="-av -/-chmod=a+rwx -n -/-exclude .git docker-run.sh ${docker.user.name}@${docker.host}:${docker.location}" />
		</exec>-->
		<exec executable="rsync" dir="${basedir}/..">
			<arg line="-av --chmod=a+rwx -n --exclude .git support-files ${docker.user.name}@${docker.host}:${docker.location}" />
		</exec>
	</target>
</project>
