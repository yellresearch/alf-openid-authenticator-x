FROM yellresearch/server:base
MAINTAINER Tan Keh Win <kehwin@yellresearch.com>

# transfer over all relevant files from host VM into container
RUN mkdir /tomcat/webapps/alfresco && mkdir /tomcat/webapps/share
#ADD support-files/ /usr/docker-install/support-files/
ADD support-files/mysql-* /tomcat/lib/
ADD alfresco-global.properties /tomcat/shared/classes/

# copy transferred files into respective locations
#RUN sudo cp -R /usr/docker-install/support-files/mysql-* /tomcat/lib/ && \
#	sudo cp -R /usr/docker-install/support-files/extension /tomcat/shared/classes/alfresco/ && \
#	sudo cp -R /usr/docker-install/support-files/web-extension /tomcat/shared/classes/alfresco/ && \
#	sudo cp -R /usr/docker-install/support-files/messages /tomcat/shared/classes/alfresco/ && \
#	sudo cp /usr/docker-install/alfresco-global.properties /tomcat/shared/classes/

# modify tomcat files
RUN sed -i -- 's/shared.loader=/shared.loader=${catalina.base}\/shared\/classes,${catalina.base}\/shared\/lib\/*.jar/g' /tomcat/conf/catalina.properties
RUN echo "export JAVA_OPTS='-server -XX:MaxPermSize=256M -Xmx2G -Xms2G -Xss1024k'" > /tomcat/bin/setenv.sh

# create contentstore folder, this can be modified using parameters
ENV dbname=alfresco
RUN mkdir /opt/alf_data/${dbname}

# prep startup scripts
ADD startup.sh /usr/docker-install/
RUN chmod 777 /usr/docker-install/startup.sh
RUN chmod 777 /tomcat/bin/catalina.sh

CMD ["/usr/docker-install/startup.sh"]




