#!/bin/bash
# Fill in DBNAME with the database name. Default value: alfresco
# Fill in CONTAINERNAME with whatever you want to name the container for ease of identification. Default value: alfcont
# If you add the parameter "build" to the script call, it will build the necessary content-store folders and image 
DBNAME=alfresco
CONTAINERNAME=alfcont
DOCKER_FOLDER=/home/<active user name>
STORAGE_HOST=
STORAGE_USERNAME=
STORAGE_FOLDER=/home/<storage host active user name>

if [ "$1" = "build" ]; then
	rsync -av --chmod=a+rwx $STORAGE_USERNAME@$STORAGE_HOST:$STORAGE_FOLDER/startup.sh .
	rsync -av --chmod=a+rwx $STORAGE_USERNAME@$STORAGE_HOST:$STORAGE_FOLDER/support-files .
	rsync -av --chmod=a+rwx $STORAGE_USERNAME@$STORAGE_HOST:$STORAGE_FOLDER/docker-run.sh .
	rsync -av --chmod=a+rwx $STORAGE_USERNAME@$STORAGE_HOST:$STORAGE_FOLDER/alfresco-global.properties .
	rsync -av --chmod=a+rwx $STORAGE_USERNAME@$STORAGE_HOST:$STORAGE_FOLDER/Dockerfile .

	dos2unix startup.sh
	dos2unix docker-run.sh
	dos2unix alfresco-global.properties
	dos2unix Dockerfile
fi
rsync -av --delete --chmod=a+rwx $STORAGE_USERNAME@$STORAGE_HOST:$STORAGE_FOLDER/alfresco .
rsync -av --delete --chmod=a+rwx $STORAGE_USERNAME@$STORAGE_HOST:$STORAGE_FOLDER/share .

if [ "$1" = "build" ]; then
	mkdir alf_data
	mkdir alf_data/$DBNAME
	chmod -R 777 alf_data
	sudo docker build -t yellresearch/server:alfresco .
else
	docker rm -f $CONTAINERNAME
	docker run -d -p 8080:8080 -p 3306:3306 -e "dbname=$DBNAME" --name=$CONTAINERNAME -v $DOCKER_FOLDER/alf_data/$DBNAME:/opt/alf_data/$DBNAME -v $DOCKER_FOLDER/share:/tomcat/webapps/share -v $DOCKER_FOLDER/alfresco:/tomcat/webapps/alfresco -v $DOCKER_FOLDER/support-files:/tomcat/shared/classes/alfresco yellresearch/server:alfresco
fi
